//
//  UserDataModel.swift
//  gitClient
//
//  Created by Константин Клинов on 2/16/19.
//  Copyright © 2019 Константин Клинов. All rights reserved.
//

import Foundation

//MARK: Declare UsersDataModel

struct UsersDataModel {
    
    var avatar: String
    var login: String
    var id: String
    
}

//MARK: Declare UserDataModel

struct UserDataModel {
    
    var avatar: String
    var login: String
    var id: String
    var name: String
    var email: String
    var organization: String
    var followingCount: String
    var followersCount: String
    var creatingDate: String
    
}


