//
//  UserCell.swift
//  gitClient
//
//  Created by Константин Клинов on 2/16/19.
//  Copyright © 2019 Константин Клинов. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var userLoginLbl: UILabel!
    @IBOutlet weak var userIDLbl: UILabel!
    @IBOutlet weak var userAvatar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
