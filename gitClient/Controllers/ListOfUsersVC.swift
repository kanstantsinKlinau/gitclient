//
//  ViewController.swift
//  gitClient
//
//  Created by Константин Клинов on 2/16/19.
//  Copyright © 2019 Константин Клинов. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SDWebImage


class ListOfUsersVC: UIViewController {
    

    //MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: Variable for data
    
    var usersData: [UsersDataModel] = []
    var refresh = UIRefreshControl()
    
    //MARK: URL
    let USERS_URL = URL(string: "https://api.github.com")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refresh.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        self.refresh.tintColor = UIColor.white
        tableView.addSubview(refresh)
        getJSON()
    }
    
    //MARK: Refresh
    
    @objc func handleRefresh(){
        tableView.reloadData()
        refresh.endRefreshing()
        
    }
    
    //MARK: Networking and work with usersJSON, pass data to usersData
        func getJSON(){
            DispatchQueue.main.async {
                Alamofire.request("https://api.github.com/users").responseJSON { response in
                    switch response.result {
                    case .success(let value):
                        let json = JSON(value)
                        json.array?.forEach({ (user) in
                            let user = UsersDataModel(avatar: user["avatar_url"].stringValue, login: user["login"].stringValue, id: user["id"].stringValue)
                            self.usersData.append(user)
                        })
                        self.tableView.reloadData()
                        case .failure(let error):
                        print(error.localizedDescription)
                    }
                }
            }
        }
}

    //MARK: TableView
    extension ListOfUsersVC: UITableViewDelegate, UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.usersData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath) as! UserCell
        cell.userIDLbl.text = "ID: \(self.usersData[indexPath.row].id)"
        cell.userLoginLbl.text = "Login: \(self.usersData[indexPath.row].login)"
        cell.userAvatar.sd_setImage(with: URL(string: self.usersData[indexPath.row].avatar))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "userDetail", sender: self)
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? UserProfileVC {
            destination.usersDetail = usersData[(tableView.indexPathForSelectedRow?.row)!]
            tableView.deselectRow(at: tableView.indexPathForSelectedRow!, animated: true)
        }
    }
}



