//
//  UserProfileVC.swift
//  gitClient
//
//  Created by Константин Клинов on 2/16/19.
//  Copyright © 2019 Константин Клинов. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import SwiftyJSON


class UserProfileVC: UIViewController {

    //MARK: Outlets
    
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var createdAtLbl: UILabel!
    @IBOutlet weak var organizationLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var followingLbl: UILabel!
    @IBOutlet weak var followersLbl: UILabel!
    
    //MARK: Variable for userData
    var usersDetail: UsersDataModel?
    var userData: [UserDataModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getUserJSON()
    
    }
    
    //MARK: Networking and work with userJSON, pass data to userData, pass data to UI
    func getUserJSON() {
        let resultURL = "https://api.github.com/users/" + (usersDetail?.login)!
        DispatchQueue.main.async {
            Alamofire.request(resultURL).responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let user = UserDataModel(avatar: json["avatar_url"].stringValue, login: json["login"].stringValue, id: json["id"].stringValue, name: json["name"].stringValue, email: json["email"].stringValue, organization: json["company"].stringValue, followingCount: json["following"].stringValue, followersCount: json["followers"].stringValue, creatingDate: json["created_at"].stringValue)
                    self.userData.append(user)
                    self.followersLbl.text = "User has \(self.userData[0].followersCount) followers"
                    self.followingLbl.text = "User has \(self.userData[0].followingCount) followings"
                    self.nameLbl.text = "Name is \(self.userData[0].name)"
                    self.createdAtLbl.text = "Date of creating is \(self.userData[0].creatingDate)"
                    self.organizationLbl.text = "Company name is \(self.userData[0].organization)"
                    self.emailLbl.text = "Email is \(self.userData[0].email)"
                    self.avatarImg.sd_setImage(with: URL(string: (self.userData[0].avatar)))
                    case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
